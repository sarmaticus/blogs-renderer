FROM node:10-alpine as development

WORKDIR /app

COPY --chown=node package*.json ./
COPY --chown=node tsconfig.json ./
COPY --chown=node healthcheck.js /healthcheck.js

RUN npm install

COPY --chown=node src src
COPY --chown=node src/themes /app/dist/themes
COPY --chown=node src/public public

RUN npm run build

## Add the wait script to the image
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.4.0/wait /wait
RUN chmod +x /wait

# Add Tini
ENV TINI_VERSION v0.18.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini-static /tini
RUN chmod +x /tini

ENTRYPOINT ["/tini", "--"]

## Launch the wait tool and then your application
CMD /wait && npm run watch

########################################################################################

FROM node:10-alpine as production

RUN mkdir -p /app && chown -R node:node /app

WORKDIR /app

COPY --chown=node --from=development /app/package*.json ./
COPY --chown=node --from=development /app/dist dist
COPY --chown=node --from=development /app/src/public public
COPY --chown=node --from=development /app/src/themes /app/dist/themes

COPY --chown=node --from=development /wait /wait
COPY --chown=node --from=development /healthcheck.js /healthcheck.js
COPY --chown=node --from=development /tini /tini

RUN npm ci --production

#### Build themes ###

# Strata
RUN npm run --prefix /app/dist/themes/strata build

# Editorial
RUN npm run --prefix /app/dist/themes/editorial build

# Imperfect
RUN npm run --prefix /app/dist/themes/imperfect build

# Clean blog
RUN npm run --prefix /app/dist/themes/clean-blog build


HEALTHCHECK --interval=30s --timeout=5s --start-period=30s --retries=5 CMD node /healthcheck.js

ENTRYPOINT ["/tini", "--"]

USER node

## Launch the wait tool and then your application
CMD /wait && npm run start