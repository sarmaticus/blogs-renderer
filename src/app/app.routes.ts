import healthApi from "../routes/health/health.routes";
import frontendApi from "../routes/frontend/frontend.routes";
import postsApi from "../routes/posts/posts.routes";
import loginApi from "../routes/login/login.routes";
import actionApi from "../routes/action/action.routes";
import commentsApi from "../routes/comments/comments.routes";
import { getBlog } from "../submodules/shared-library/services/cache/cache";
import { endpointLogger } from "../submodules/shared-library/utils/logger";
import { Response } from 'express';

function routes(app:any) {

    app.locals.moment = require('moment');

    app.use('/health', healthApi);
    app.use('/posts', postsApi);
    app.use('/login', endpointLogger, loginApi);
    app.use('/action', endpointLogger, actionApi);
    app.use('/comments', commentsApi);
    app.use('/', frontendApi);
    app.use('*', (req: Request, res: Response) => res.redirect('/'))

    app.use( async (err:any, req:any, res:any, next:any) => {

        const {hostname} = req;
        const blog = await getBlog(hostname);

        res.locals.message = err.message;
        res.locals.error = err;

        // render the error page
        res.status(err.status || 500);
        res.render(`error`);
    });
}

export default routes;
