/*
	Stellar by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
*/

(function($) {

	var	$window = $(window),
		$body = $('body'),
		$main = $('#main');

		$window.on('load', function() {
			window.setTimeout(function() {
				$body.removeClass('is-preload');
			}, 100);
		});

		var $nav = $('#nav');

		if ($nav.length > 0) {

			$main
				.scrollex({
					mode: 'top',
					enter: function() {
						$nav.addClass('alt');
					},
					leave: function() {
						$nav.removeClass('alt');
					},
				});
		}

		$('.scrolly').scrolly({
			speed: 1000
		});




		////////////





	let skip = 12;

	if($("#main").children().length < 12) {
		$('.center-button').hide();
	}

	$("#load-more").click(function (e) {
		e.preventDefault();

		const categoryId = $('#categoryId_indicator').val();
		const blogId = $('#blogId_indicator').val();

		$('.center-button').hide();

		$.ajax({
			type: "GET",
			beforeSend: function(request) {
				request.setRequestHeader("blogId", blogId);
				request.setRequestHeader("skip", skip);

				if(categoryId) request.setRequestHeader("categoryId", categoryId);
			},
			url: "/posts",

			success: function (data) {

				skip = skip + 12;

				if(data.posts) {
					data.posts.map(function (article) {
						const articleBox = generateNewArticleBox(article);
						$('#main').append(articleBox);
					});

					if(data.posts.length == 12) {
						$('.center-button').show();
					}
				}
			},
			error: function (data) {
				console.log(data);
				toastr.error("Something gone wrong...");
			}
		});

	});


	const generateNewArticleBox = (article) => {
		return `
<section class="main" id="intro">
    <div class="spotlight">
        <div class="content">
            <header class="major">
                <a href="/${article.permlink}"><h2>${article.title}</h2></a>
            </header>
            <p>${article.abstract}</p>
            <ul class="actions">
                <li><a class="button primary" href="/${article.permlink}">Read</a></li>
                <li><div class="button votes">Votes: ${article.total_votes}</div></li>
                <li><div class="button reward">Reward: $${article.value.toFixed(2)}</div></li>
            </ul>
        </div>
        <span class="image"><img alt="" src="${article.thumbnail_image ? article.thumbnail_image : '/assets/img/default.jpg'}"/></span>
    </div>
</section>
`;
	}



})(jQuery);
