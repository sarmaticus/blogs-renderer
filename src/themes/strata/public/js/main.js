import Vue from 'vue';

import SSO from './vue/components/sections/SSO';
import LoadMore from './vue/components/sections/LoadMoreSection'
import Comments from './vue/components/sections/CommentsSection';
import ArticleActionsSection from "./vue/components/sections/ArticleActionsSection";

import store from './vue/store/store';
import VTooltip from 'v-tooltip'

Vue.use(require('vue-moment'));
Vue.use(VTooltip)

new SSO({
    store
}).$mount('#sso')

new Comments({
    store
}).$mount('#comments');

new LoadMore({
    store
}).$mount('#articles');

new ArticleActionsSection({
    store
}).$mount('#article-actions');
