toastr.options.positionClass = "toast-bottom-right";

$(document).ready(function () {

    let skip = 12;
    var blogId = $('#blogId_indicator').val();

    if($("#posts").children().length < 12) {
        $('.center-button').hide();
    }

    $(".load-more").click(function (e) {
        e.preventDefault();
        
        var categoryId = $('#categoryId_indicator').val();
    
        $('.center-button').hide();

        $.ajax({
            type: "GET",
            beforeSend: function(request) {
                request.setRequestHeader("blogId", blogId);
                request.setRequestHeader("skip", skip);
                
                if(categoryId) request.setRequestHeader("categoryId", categoryId);
            },
            url: "/posts",
         
            success: function (data) {

                skip = skip + 12;

                if(data.posts) {
                    data.posts.map(function (post) {
                        var x = document.createElement('div');
                        $(x).addClass('col-lg-4').addClass('col-md-6');
                        $(x).append(generateNewPostInfo(post));
                        $('#posts').append(x);
                    });

                    if(data.posts.length == 12) {
                        $('.center-button').show();
                    }
                }
            },
            error: function (data) {
                console.log(data);
                toastr.error("Something gone wrong...");
            }
        });

    });

});

function generateNewPostInfo(article) {

    const generateCategoriesList = (categories) => categories.reduce( (result, category) => result + `<a class="category" href="/category/${category.slug}">${category.name}</a>`, "");
    const value = article.value.toFixed(2);
    return `
<div class="news-post standart-post">
    <div class="post-image">
        <a href="/${article.permlink}"><img src="${article.thumbnail_image ? article.thumbnail_image :'/img/default.jpg'}"/></a>
        <span class="categories">${generateCategoriesList(article.categories)}</span>
    </div>
    <h2><a href="/${article.permlink}">${article.title}</a></h2>
    <ul class="post-tags">
        <li><div><i class="lnr lnr-user"></i>@${article.author}</div></li>
        <li><a href="/${article.permlink}#comments"><i class="lnr lnr-bubble"></i><span>${article.total_votes}</span></a></li>
        <li><i class="lnr lnr-thumbs-up"></i>${article.comments}</li>
        <li><i class="lnr lnr-diamond"></i>$${value}</li>
    </ul>
    <p>${article.abstract}</p>
</div>
`;

}
