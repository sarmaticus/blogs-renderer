import app from './app/app';
import { listenOnPort } from './submodules/shared-library/utils/listenOnPort';
import { waitForMicroservice, microservices, mongo, logger } from './submodules/shared-library';
import * as mongoose from 'mongoose';

( async () => {

    try {
        await mongoose.connect(mongo.uri, mongo.options);
    
        await waitForMicroservice(microservices.auth);
        await waitForMicroservice(microservices.sitemap_builder);
    
        listenOnPort(app, 3000);
        
    } catch (error) {
        logger.error('Error encountered while starting the app: ', error.message);    
        process.exit(1);
    }

})();
