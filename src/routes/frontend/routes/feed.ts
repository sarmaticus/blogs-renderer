import { Request, Response } from 'express';
import { handleResponseError, logger } from '../../../submodules/shared-library';
import { getBlog, getLatestArticles } from '../../../submodules/shared-library/services/cache/cache';
import sitemap from '../../../services/sitemap/sitemap.service';

const FEED_ARTICLES_AMOUNT = 50;

const middleware: any[] =  [];

async function handler(req: Request, res: Response) {

    return handleResponseError(async () => {

        const { hostname } = req;

        const blog = await getBlog(hostname);

        if(blog.domain_redirect && blog.custom_domain && hostname != blog.custom_domain) {
            return res.redirect(`https://${blog.custom_domain}/feed`);
        }

        const articles = await getLatestArticles(blog._id, 0, FEED_ARTICLES_AMOUNT);

        return res.type('xml').render(`feed.pug`, {
            blog,
            articles
        });

    }, req, res);
}

export default {
    middleware,
    handler
}
