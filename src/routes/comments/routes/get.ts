import { Request, Response } from 'express';
import { handleResponseError, hive } from '../../../submodules/shared-library';
import { body } from 'express-validator/check';
import renderSteemCommentBody from '../../../submodules/shared-library/services/article/renderSteemCommentBody';
import { getBlog } from '../../../submodules/shared-library/services/cache/cache';

const middleware: any[] =  [
    body('author').isString(),
    body('permlink').isString()
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {

        const {author, permlink} = req.body;

        const blog = await getBlog(req.hostname);

        const rawReplies = await hive.api.getContentRepliesAsync( author, permlink);

        blog.hidden_commenters = blog.hidden_commenters ? blog.hidden_commenters: [];

        const filteredReplies = rawReplies.filter((reply: any) => blog.hidden_commenters.indexOf(reply.author) === -1);

        const replies = await Promise.all(filteredReplies.map( async (comment: any) => {
            comment.rendered = await renderSteemCommentBody(comment.body);
            return comment;
        }));

        return res.json(replies);

    }, req, res);
}

export default {
    middleware,
    handler
}
